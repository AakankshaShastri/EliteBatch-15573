isqrt = floor.sqrt.fromIntegral
factors n = [x | x <- [1..n], n `mod` x == 0]
isprime n = factors n == [1,n]
primes = 2: [x | x <- [3..],isprime(x)]
val = primes !! 10001
main = do putStrLn $ show val
 

