fibonacci = 0:1:(zipWith (+) fibonacci (tail fibonacci))
index = 10^999
prob25 = length term where term = takeWhile (< index) fibonacci
main = do 
       print $ prob25
