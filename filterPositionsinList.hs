f :: [Int] -> [Int]
f lst = [x | i <- [1,3..length lst],let x = lst !! i]
main = do
   inputdata <- getContents
   mapM_ (putStrLn. show). f. map read. lines $ inputdata
