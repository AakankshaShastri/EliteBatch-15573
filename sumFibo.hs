f = sum[x|x <- takeWhile(<=1000000) fi,even x] where fi = 0:1:(zipWith(+) fi (tail fi))
main = putStrLn $ show f