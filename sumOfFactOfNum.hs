fact 0 = 1
fact n = n * fact(n-1)
sumd 0 = 0
sumd x = x `rem` 10 + sumd(x `div` 10)
main = do
       num <- readLn
       print $ sumd $ fact num
