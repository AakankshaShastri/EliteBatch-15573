countOfElem elem list = length $ filter (\x -> x == elem) $ list
score x
    | x == 1 = 15
    | x == 2 = 30
    | x == 3 = 40
compareScores = if (score (countOfElem 'a' "abab") - score (countOfElem 'b' "abab") >= 2) then "A is Winner" else "B is Winner"
checkIfEqual  =  if (score (countOfElem 'a' "abab") == score (countOfElem 'b' "abab")) then "Duece" else compareScores
main = do 
       print $ score (countOfElem 'a' "abab")
       print $ score (countOfElem 'b' "abab")
       putStrLn $ show checkIfEqual 