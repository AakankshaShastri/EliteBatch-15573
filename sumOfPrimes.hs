factors n = [x|x <- [1..n],n `mod` x == 0]
isprime i = factors i == [1,i]
primes = 2 : [x|x <- [3..200000],isprime x]
result = sum(primes)
main = do putStrLn $ show result 