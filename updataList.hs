f arr = [absolute(x)| x <- arr]
absolute x 
            |x < 0   =  -x
            | otherwise =  x
-- This section handles the Input/Output and can be used as it is. Do not modify it.
main = do
   inputdata <- getContents
   mapM_ putStrLn $ map show $ f $ map (read :: String -> Int) $ lines inputdata
