getMultipleLines n 
     | n <= 0 = return []
     | otherwise = do
        let x = "hello" 
        xs <- getMultipleLines (n-1) 
        let v =  (x : xs)
        return v

main = do 
       number <- readLn
       getMultipleLines number 
        