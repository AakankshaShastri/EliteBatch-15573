isdig 0 = []
isdig n = n `rem` 10 : isdig(n `div` 10)
fifthSum n = sum[x^5 | x <- isdig n]
isFifth n = n == fifthSum n
listOfAllFifth = sum[x | x <- [2..1000000],isFifth x]  
main = do
       print $ listOfAllFifth 
