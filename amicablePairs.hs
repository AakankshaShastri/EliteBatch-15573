amics = sum[x | x <- [1..500],y <- [1..500],isAmicable x y]
factorsSum n = sum[x | x <- [1..n-1],n `rem` x == 0]
isAmicable num1 num2 = (num1 == factorsSum num2) && (num2 == factorsSum num1)
main = do 
       print $ amics
