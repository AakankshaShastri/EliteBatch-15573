import Data.List.Split
compareTotal input = [x| x <- (splitOn "," input), isAGreater x]
countOfElem elem list = length $ filter (\x -> x == elem) $ list
score x
    | x == 1 = 15
    | x == 2 = 30
    | x == 3 = 40
--compareScores input = score (countOfElem 'a' input) - score (countOfElem 'b' input)
--checkIfEqual input =  score (countOfElem 'a' input) == score (countOfElem 'b' input)
isAGreater input = score (countOfElem 'a' input) > score (countOfElem 'b' input)
isBGreater input = score (countOfElem 'a' input) < score (countOfElem 'b' input)
main = do
       input <- getLine
       print $ score (countOfElem 'a' input)
       print $ score (countOfElem 'b' input)
       putStrLn $ show $ compareTotal input