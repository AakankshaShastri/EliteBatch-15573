fact 0 = 1
fact n = n * fact(n-1)
digits 0 = []
digits x = x `mod` 10 : digits(x `div` 10)
sumOfFact n = sum[fact x | x <- digits n]  
main = do
       n <- readLn
       print $ sumOfFact n
