isqrt = floor.sqrt.fromIntegral
isFactor n x = n `mod` x == 0
isComposite n = or $ map(isFactor n)[2..isqrt n]
primeFactors n = last[x | x <- [2..isqrt n],n `mod` x == 0,not(isComposite x)]
main = do print $ primeFactors 600851475143
