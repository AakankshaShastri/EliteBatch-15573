sumd 0 = 0
sumd x = (x `mod` 10) + sumd (x `div` 10)
power x = 2 ** x
main = do
       n <- readLn
       print $ sumd $ round $ power n
